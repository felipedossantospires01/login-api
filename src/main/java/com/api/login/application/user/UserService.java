package com.api.login.application.user;

import com.api.login.domain.user.Role;
import com.api.login.domain.user.User;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if(user == null){
            log.error("Usuario não existe");
            throw  new UsernameNotFoundException("Usuario não existe");
        }else{

        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach( role -> {
                    authorities.add(new SimpleGrantedAuthority(role.getName()));
                });
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }

    public User saveUser(User user){
        log.info("Salvando usuario: " + user.getName());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }
    public Role saveRole(Role role){
        log.info("Salvando nova permissao: " + role.getName());
        return roleRepository.save(role);
    }
    public void addRoleToUser(String username, String roleName){
        log.info("Adicionando nova permissao: " + roleName + " para o usuario: " + username);
        User user = userRepository.findByUsername(username);
        Role role = roleRepository.findByName(roleName);
        user.getRoles().add(role);
    }
    public User getUser(String username){
        log.info("Buscando usuario:" + username);
        return userRepository.findByUsername(username);
    }
    public List<User> getUsers(){
        log.info("Buscando lista de usuarios:");
          return userRepository.findAll().stream().map(user -> {
            user.setPassword(null);
            return user;
        }).collect(Collectors.toList());
    }
}
