package com.api.login;

import com.api.login.application.user.UserService;
import com.api.login.domain.user.Role;
import com.api.login.domain.user.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class LoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoginApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

	@Bean
	CommandLineRunner run(UserService userService){
		return args ->{
			userService.saveRole(new Role(null, "ROLE_USER", new ArrayList<>()));
			userService.saveRole(new Role(null,"ROLE_ADMIN", new ArrayList<>()));
			userService.saveRole(new Role(null,"ROLE_SUPER_ADMIN", new ArrayList<>()));
			userService.saveRole(new Role(null,"ROLE_MANAGER", new ArrayList<>()));

			userService.saveUser(new User(null, "Felipe Pires", "felipe.pires", "12345678", new ArrayList<>()));
			userService.saveUser(new User(null, "Maria Pires", "maria.pires", "12345678", new ArrayList<>()));
			userService.saveUser(new User(null, "Itamar Pires", "itamar.pires", "12345678", new ArrayList<>()));
			userService.saveUser(new User(null, "Alessandra Pires", "alessandra.pires", "12345678", new ArrayList<>()));

			userService.addRoleToUser("felipe.pires", "ROLE_SUPER_ADMIN");
			userService.addRoleToUser("maria.pires", "ROLE_ADMIN");
			userService.addRoleToUser("itamar.pires", "ROLE_MANAGER");
			userService.addRoleToUser("alessandra.pires", "ROLE_USER");
		};
	}
}
